if(rides.length>0){//si existen rides estonces se cargan
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    var salida = urlParams.get('salida');
    var llegada = urlParams.get('llegada');
    if(salida != null){
        if(salida.length == 0){
            salida = null;
        }else{
            document.getElementById("salida").value = salida;
            salida = salida.toLowerCase();
        }
    }
    if(llegada != null){
        if(llegada.length == 0){
            llegada = null;
        }else{
            document.getElementById("llegada").value = llegada;
            llegada = llegada.toLowerCase();
        }
    }
    var tabla = '<table>'+
        '<tr>'+
            '<th>Usuario</th>'+
            '<th>Nombre</th>'+
            '<th>Salida</th>'+
            '<th>Llegada</th>'+
            '<th>Ver</th>'+
        '</tr>';
    rides.forEach(function(value, index, array) {
        var salidaA = array[index][3].toLowerCase();
        var llegadaA = array[index][4].toLowerCase();
        if(salida != null && llegada != null){
            if(salida == salidaA && llegada == llegadaA){
                tabla += '<tr>'+
                    '<td>'+array[index][1]+'</td>'+
                    '<td>'+array[index][2]+'</td>'+
                    '<td>'+array[index][3]+'</td>'+
                    '<td>'+array[index][4]+'</td>'+
                    '<td><img onclick="verRide('+array[index][0]+')" class="btnTablaRide" src="img/ver.png" alt=""></td>'+
                '</tr>';
            }
        }else if(salida != null && llegada == null){
            if(salida == salidaA){
                tabla += '<tr>'+
                    '<td>'+array[index][1]+'</td>'+
                    '<td>'+array[index][2]+'</td>'+
                    '<td>'+array[index][3]+'</td>'+
                    '<td>'+array[index][4]+'</td>'+
                    '<td><img onclick="verRide('+array[index][0]+')" class="btnTablaRide" src="img/ver.png" alt=""></td>'+
                '</tr>';
            }
        }else if(salida == null && llegada != null){
            if(llegada == llegadaA){
                tabla += '<tr>'+
                    '<td>'+array[index][1]+'</td>'+
                    '<td>'+array[index][2]+'</td>'+
                    '<td>'+array[index][3]+'</td>'+
                    '<td>'+array[index][4]+'</td>'+
                    '<td><img onclick="verRide('+array[index][0]+')" class="btnTablaRide" src="img/ver.png" alt=""></td>'+
                '</tr>';
            }
        }else{
            tabla += '<tr>'+
                '<td>'+array[index][1]+'</td>'+
                '<td>'+array[index][2]+'</td>'+
                '<td>'+array[index][3]+'</td>'+
                '<td>'+array[index][4]+'</td>'+
                '<td><img onclick="verRide('+array[index][0]+')" class="btnTablaRide" src="img/ver.png" alt=""></td>'+
            '</tr>';
        }
    });
    tabla += '</table>';
    document.getElementById("contenedorRidesPublico").innerHTML = tabla;
}
document.getElementById("btnbuscar").onclick = function () { buscarRide(); }

//funcion para ver ride publico
function verRide(id){
    window.location.href = "formulario_rides.html?opcion=ver&id="+id;
}

//funcion para buscar ride
function buscarRide(){
    var salida = document.getElementById("salida").value;
    var llegada = document.getElementById("llegada").value;
    window.location.href = "rides.html?salida="+salida+"&llegada="+llegada;
}