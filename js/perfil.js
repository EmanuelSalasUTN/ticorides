//funcion para primer boton de registro, muestra formulario de registro
document.getElementById("btn_registrar1").onclick = function() { formularioRegistro(0,0); }
document.getElementById("btn_iniciar1").onclick = function() { formularioInicio(); }
//validar usuario si existe
var usuarios = [];
var usuarioActualizar = [];
var usuariosSesion = JSON.parse(sessionStorage.getItem('usuarios'));
if(usuariosSesion == null){
    //usuario no definido
}else{
    //si existe un usuario, mostrar datos
    usuarios = usuariosSesion;
    usuarios.forEach(verificarSesionUsuarios);
}
function verificarSesionUsuarios(value, index, array) {
    if(value[7] == 1){//usuario logueado entonces se cargan datos
        formularioRegistro(1,array[index]);
    }
}

function formularioRegistro(cargarDatos,usuario){
    document.getElementById("containerperfil").innerHTML = '<div>'+
        '<img src="img/usuario.png">'+
    '</div>'+
    '<form class="containerregistro" action="" method="POST" name="registro" id="registro">'+
        '<div>'+
            '<input class="inputperfil" name="nombre" type="text" placeholder="nombre*">'+
        '</div>'+
        '<div>'+
            '<input class="inputperfil" name="apellidos" type="text" placeholder="apellidos">'+
       '</div>'+
        '<div>'+
            '<input class="inputperfil" name="edad" min="1" step="1" type="number" placeholder="edad">'+
        '</div>'+
        '<div>'+
            '<select name="genero" class="selectperfil">'+
                '<option value="0" disabled selected>Género</option>'+
                '<option value="Masculino">Masculino</option>'+
                '<option value="Femenino">Femenino</option>'+
                '<option value="Prefiero no decirlo">Prefiero no decirlo</option>'+
            '</select>'+
        '</div>'+
        '<div>'+
            '<input class="inputperfil" name="email" type="email" placeholder="email*">'+
        '</div>'+
        '<div>'+
            '<input class="inputperfil" name="usuario" type="text" placeholder="nombre de usuario*">'+
        '</div>'+
        '<div>'+
            '<input class="inputperfil" name="pass" type="password" placeholder="contraseña*">'+
        '</div>'+
        '<div>'+
            '<input class="inputperfil" name="confirmarpass" type="password" placeholder="repetir contraseña*">'+
        '</div>'+
        '<div id="contenedor_mensajerequeridos">'+
            '<label id="datos_requeridos"></label>'+
        '</div>';
    //validar si es registro o cargar datos de usuario actual
    if(cargarDatos == 1){
        document.getElementById("containerperfil").innerHTML += '<button type="button" id="btn_guardardatos" class="botonperfil2">Actualizar</button>';
    }else{
        document.getElementById("containerperfil").innerHTML += '<button type="button" id="btn_registrarse" class="botonperfil2">Registrarse</button>'+
        '<button type="button" id="btn_atras" class="botonperfil2">Atrás</button>';
    }
    document.getElementById("containerperfil").innerHTML += '</form>';
    //asignar metodos a los botones
    if(cargarDatos == 1){
        document.getElementById("btn_guardardatos").onclick = function () { guardarDatos("actualizar"); }
        cargarDatosUsuario(usuario);
    }else{
        document.getElementById("btn_atras").onclick = function() { btnAtras(); }
        document.getElementById("btn_registrarse").onclick = function () { guardarDatos("crear"); }
    }
    
}
//funcion para cargar datos del usuario
function cargarDatosUsuario(usuario){
    document.getElementById("perfil").innerHTML = usuario[5];
    document.getElementsByName("nombre")[0].value = usuario[0];
    document.getElementsByName("apellidos")[0].value = usuario[1];
    document.getElementsByName("edad")[0].value = usuario[2];
    document.getElementsByName("genero")[0].value = usuario[3];
    document.getElementsByName("email")[0].value = usuario[4];
    document.getElementsByName("usuario")[0].value = usuario[5];
    document.getElementsByName("usuario")[0].disabled = true;
    document.getElementsByName("pass")[0].value = usuario[6];
    document.getElementsByName("confirmarpass")[0].value = usuario[6];
}

//funcion para registrar usuario
function guardarDatos(opcion){
    document.getElementById("datos_requeridos").innerHTML = "";
    var inputs = document.getElementById("registro").elements;
    var nombre = inputs["nombre"].value;
    var apellidos = inputs["apellidos"].value;
    var edad = inputs["edad"].value;
    var genero = inputs["genero"].value;
    var email = inputs["email"].value;
    var usuario = inputs["usuario"].value;
    var pass = inputs["pass"].value;
    var confirmarpass = inputs["confirmarpass"].value;
    var paso = 1;
    //validar nombre
    if(nombre.length <= 0){
        document.getElementsByName("nombre")[0].classList.add("inputerror");
        document.getElementById("datos_requeridos").innerHTML += "*Debe de ingresar su nombre";
        paso = 0;
    }else{
        document.getElementsByName("nombre")[0].classList.remove("inputerror");
    }
    //validar nombre de usuario
    if(usuario.length <= 0){
        document.getElementsByName("usuario")[0].classList.add("inputerror");
        document.getElementById("datos_requeridos").innerHTML += "<br>*Debe de ingresar un nombre de usuario";
        paso = 0;
    }else{
        document.getElementsByName("usuario")[0].classList.remove("inputerror");
    }
    //validar email
    if(email.length <= 0){
        document.getElementsByName("email")[0].classList.add("inputerror");
        document.getElementById("datos_requeridos").innerHTML += "<br>*Debe de ingresar un email";
        paso = 0;
    }else{
        document.getElementsByName("email")[0].classList.remove("inputerror");
    }
    //validar pass
    if(pass != confirmarpass || pass.length == 0){
        document.getElementsByName("pass")[0].classList.add("inputerror");
        document.getElementsByName("confirmarpass")[0].classList.add("inputerror");
        document.getElementById("datos_requeridos").innerHTML += "<br>*Verificar contraseña";
        paso = 0;
    }else{
        document.getElementsByName("pass")[0].classList.remove("inputerror");
        document.getElementsByName("confirmarpass")[0].classList.remove("inputerror");
    }
    if(paso == 1){
        if(opcion == "crear"){
            var datosusuario = [];
            datosusuario[0] = nombre;
            datosusuario[1] = apellidos;
            datosusuario[2] = edad;
            datosusuario[3] = genero;
            datosusuario[4] = email;
            datosusuario[5] = usuario;
            datosusuario[6] = pass;
            datosusuario[7] = 0;
            usuarios.push(datosusuario);
            //realizar registro
            sessionStorage.setItem('usuarios', JSON.stringify(usuarios));
            alert("Datos guardados con éxito!");
            window.location.href = "perfil.html";
        }else{//actualizar
            usuarioActualizar = [];
            usuarioActualizar[0] = nombre;
            usuarioActualizar[1] = apellidos;
            usuarioActualizar[2] = edad;
            usuarioActualizar[3] = genero;
            usuarioActualizar[4] = email;
            usuarioActualizar[5] = usuario;
            usuarioActualizar[6] = pass;
            usuarioActualizar[7] = 1;
            usuarios.forEach(actualizarDatosUsuario);
            //realizar update
            sessionStorage.setItem('usuarios', JSON.stringify(usuarios));
            alert("Datos guardados con éxito!");
            window.location.href = "perfil.html";
        }
    }
}

function actualizarDatosUsuario(value, index, array) {
    if(value[7] == 1){//usuario logueado entonces se actualizan datos
        array[index][0] = usuarioActualizar[0];
        array[index][1] = usuarioActualizar[1];
        array[index][2] = usuarioActualizar[2];
        array[index][3] = usuarioActualizar[3];
        array[index][4] = usuarioActualizar[4];
        array[index][5] = usuarioActualizar[5];
        array[index][6] = usuarioActualizar[6];
        array[index][7] = usuarioActualizar[7];
    }
}

function actualizarUsuario(value, index, array) {
    if(value[5] == 1){//usuario logueado entonces se cargan datos
        formularioRegistro(1,array[index]);
    }
}


//funcion para ir hacia atras y mostrar opciones de registro y inicio de sesion
function btnAtras(){
    //funcion del btn atras para regresar a los botones originales de registrar y login
    document.getElementById("containerperfil").innerHTML = '<div>'+
            '<img src="img/usuario.png">'+
        '</div>'+
        '<div>'+
            '<button type="button" id="btn_registrar1" class="botonperfil">Registrarse</button>'+
        '</div>'+
        '<div>'+
            '<button type="button" id="btn_iniciar1" class="botonperfil">Iniciar Sesión</button>'+
        '</div>';
    document.getElementById("btn_registrar1").onclick = function() { formularioRegistro(0,0); }
    document.getElementById("btn_iniciar1").onclick = function() { formularioInicio(); }
}

//formulario de inicio de sesion
function formularioInicio(){
    document.getElementById("containerperfil").innerHTML = '<div>'+
        '<img src="img/usuario.png">'+
    '</div>'+
    '<form class="containerinicio" action="" method="POST" name="inicio" id="inicio">'+
        '<div>'+
            '<input class="inputperfil" name="usuario" type="text" placeholder="nombre de usuario">'+
        '</div>'+
        '<div>'+
            '<input class="inputperfil" name="pass" type="password" placeholder="contraseña">'+
        '</div>'+
        '<div id="contenedor_mensajerequeridos">'+
            '<label id="datos_requeridos"></label>'+
        '</div>'+
        '<button type="button" id="btn_iniciar_sesion" class="botonperfil2">Iniciar Sesión</button>'+
        '<button type="button" id="btn_atras" class="botonperfil2">Atrás</button>'+
    '</form>';
    document.getElementById("btn_atras").onclick = function() { btnAtras(); }
    document.getElementById("btn_iniciar_sesion").onclick = function() { iniciarSesion(); }
}

//funcion para iniciar sesion
function iniciarSesion(){
    var cantidadUsuarios = usuarios.length;
    document.getElementById("datos_requeridos").innerHTML = "";
    var paso = 1;
    var usuarioInicio = document.getElementsByName("usuario")[0].value;
    //validar nombre de usuario
    if(usuarioInicio.length <= 0){
        document.getElementsByName("usuario")[0].classList.add("inputerror");
        document.getElementById("datos_requeridos").innerHTML += "<br>*Debe de ingresar el nombre de usuario";
        paso = 0;
    }else{
        document.getElementsByName("usuario")[0].classList.remove("inputerror");
    }
    var passInicio = document.getElementsByName("pass")[0].value;
    //validar clave
    if(passInicio.length <= 0){
        document.getElementsByName("pass")[0].classList.add("inputerror");
        document.getElementById("datos_requeridos").innerHTML += "<br>*Debe de ingresar la contraseña";
        paso = 0;
    }else{
        document.getElementsByName("pass")[0].classList.remove("inputerror");
    }
    if(cantidadUsuarios == 0){
        alert("Usuario/contraseña incorrecto");
        return;
    }
    if(paso == 1){
        try {
            var cantidad = usuarios.length - 1;
            var loguea = 0;
            usuarios.forEach(function(value, index, array) {
                if(value[5] == usuarioInicio && value[6] == passInicio){//usuario logueado entonces se cargan datos
                    loguea = 1;
                    array[index][7] = 1;
                    sessionStorage.setItem('usuarios', JSON.stringify(usuarios));
                    //formularioRegistro(1,array[index]);
                    window.location.href = "perfil.html";
                }
                if (cantidad === index) throw BreakException;
            });
        } catch (e) {
            if(loguea == 0){
                alert("Usuario/contraseña incorrecto");
            }
        }
    }
}

