if(usuarioLogueado.length == 0){
    window.location.href = "index.html";
}
document.getElementById("btnAgregarRide").onclick = function() { window.location.href = "formulario_rides.html?opcion=crear"; }
var ridesUsuario = 0;
if(rides.length>0){//si existen rides estonces se cargan
    var tabla = '<table>'+
        '<tr>'+
            '<th>Nombre</th>'+
            '<th>Salida</th>'+
            '<th>Llegada</th>'+
            '<th>Editar</th>'+
            '<th>Borrar</th>'+
        '</tr>';
    rides.forEach(function(value, index, array) {
        if(array[index][1] == usuarioLogueado[5]){
            tabla += '<tr>'+
                    '<td>'+array[index][2]+'</td>'+
                    '<td>'+array[index][3]+'</td>'+
                    '<td>'+array[index][4]+'</td>'+
                    '<td><img onclick="editarRide('+array[index][0]+')" class="btnTablaRide" src="img/editar.png" alt=""></td>'+
                    '<td><img onclick="eliminarRide('+array[index][0]+')" class="btnTablaRide" src="img/eliminar.png" alt=""></td>'+
                '</tr>';
            ridesUsuario++;
        }
    });
    if(ridesUsuario > 0){
        tabla += '</table>';
        document.getElementById("contenedorRidesUsuario").innerHTML = tabla;
    }
}

function editarRide(id){
    window.location.href = "formulario_rides.html?opcion=editar&id="+id;
}

function eliminarRide(id){
    var r = confirm("Seguro que desea eliminar el ride seleccionado?");
    if(r == true){
        rides.forEach(function(value, index, array) {
            if(id == array[index][0]){
                array.splice(index, 1);
            }
        });
        sessionStorage.setItem('rides', JSON.stringify(rides));
        alert("Ride eliminado con éxito!");
        window.location.href = "dashboard.html";
    }
}