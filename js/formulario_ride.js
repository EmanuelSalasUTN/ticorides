//cargar rides existentes
var id = 0;
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const opcion = urlParams.get('opcion');
if(opcion == "crear"){
    document.getElementById("legendride").innerHTML = 'Crear Ride';
}else if(opcion == "editar"){
    document.getElementById("legendride").innerHTML = 'Modificar Ride';
    //cargar datos de ride para modificar
    id = urlParams.get('id');
    rides.forEach(function(value, index, array) {
        if(id == array[index][0]){
            document.getElementById("name").value = array[index][2];
            document.getElementById("salida").value = array[index][3];
            document.getElementById("llegada").value = array[index][4];
            document.getElementById("descripcion").value = array[index][5];
            document.getElementById("horaSalida").value = array[index][6];
            document.getElementById("horaLlegada").value = array[index][7];
            var checkboxC = document.getElementsByClassName("dias");
            for (var i = 0; i < checkboxC.length; i++) {
                array[index][8].forEach(function(valorDia, index, array) {
                    if (checkboxC[i].value == valorDia) {
                        checkboxC[i].checked = true;
                    }
                });
            }
        }
    });
}else{
    document.getElementById("legendride").innerHTML = 'Ver Ride';
    //cargar datos de ride para modificar
    id = urlParams.get('id');
    rides.forEach(function(value, index, array) {
        if(id == array[index][0]){
            document.getElementById("name").value = array[index][2];
            document.getElementById("salida").value = array[index][3];
            document.getElementById("llegada").value = array[index][4];
            document.getElementById("descripcion").value = array[index][5];
            document.getElementById("horaSalida").value = array[index][6];
            document.getElementById("horaLlegada").value = array[index][7];
            var checkboxC = document.getElementsByClassName("dias");
            for (var i = 0; i < checkboxC.length; i++) {
                array[index][8].forEach(function(valorDia, index, array) {
                    if (checkboxC[i].value == valorDia) {
                        checkboxC[i].checked = true;
                    }
                });
            }
        }
    });
    document.getElementById("btnguardarride").remove();
    document.getElementsByClassName("active")[0].classList.remove("active");
    document.getElementById("menuRideli").classList.add("active");
}
if(opcion == "crear" || opcion == "editar"){
    document.getElementById("btnguardarride").onclick = function() { guardarRide(); }
    document.getElementById("btncancelarride").onclick = function() { window.location.href = "dashboard.html"; }
}else{
    document.getElementById("btncancelarride").onclick = function() { window.location.href = "rides.html"; }
}
//function para guardar ride
function guardarRide(){
    var paso = 1;
    var inputs = document.getElementById("formride").elements;
    var nombre = inputs["name"].value;
    //validar nombre
    if(nombre.length <= 0){
        document.getElementById("name").classList.add("inputerror");
        paso = 0;
    }else{
        document.getElementById("name").classList.remove("inputerror");
    }
    var salida = inputs["salida"].value;
    //validar salida
    if(salida.length <= 0){
        document.getElementById("salida").classList.add("inputerror");
        paso = 0;
    }else{
        document.getElementById("salida").classList.remove("inputerror");
    }
    var llegada = inputs["llegada"].value;
    //validar llegada
    if(llegada.length <= 0){
        document.getElementById("llegada").classList.add("inputerror");
        paso = 0;
    }else{
        document.getElementById("llegada").classList.remove("inputerror");
    }
    var descripcion = inputs["descripcion"].value;
    var horaSalida = inputs["horaSalida"].value;
    //validar horaSalida
    if(horaSalida.length <= 0){
        document.getElementById("horaSalida").classList.add("inputerror");
        paso = 0;
    }else{
        document.getElementById("horaSalida").classList.remove("inputerror");
    }
    var horaLlegada = inputs["horaLlegada"].value;
    //validar horaLlegada
    if(horaLlegada.length <= 0){
        document.getElementById("horaLlegada").classList.add("inputerror");
        paso = 0;
    }else{
        document.getElementById("horaLlegada").classList.remove("inputerror");
    }
    var dias = [];
    var checkboxes = document.querySelectorAll('input[type=checkbox]:checked');
    for (var i = 0; i < checkboxes.length; i++) {
        dias.push(checkboxes[i].value);
    }
    if(paso == 1){
        if(opcion == "crear"){
            var datosride = [];
            datosride[0] = consecutivo;
            datosride[1] = usuarioLogueado[5];
            datosride[2] = nombre;
            datosride[3] = salida;
            datosride[4] = llegada;
            datosride[5] = descripcion;
            datosride[6] = horaSalida;
            datosride[7] = horaLlegada;
            datosride[8] = dias;
            rides.push(datosride);
            //realizar registro
            sessionStorage.setItem('rides', JSON.stringify(rides));
            alert("Datos guardados con éxito!");
            window.location.href = "dashboard.html";
        }else{//actualizar
            rides.forEach(function(value, index, array) {
                if(value[0] == id){
                    array[index][0] = consecutivo;
                    array[index][1] = usuarioLogueado[5];
                    array[index][2] = nombre;
                    array[index][3] = salida;
                    array[index][4] = llegada;
                    array[index][5] = descripcion;
                    array[index][6] = horaSalida;
                    array[index][7] = horaLlegada;
                    array[index][8] = dias;
                }
            });
            sessionStorage.setItem('rides', JSON.stringify(rides));
            alert("Datos guardados con éxito!");
            window.location.href = "dashboard.html";
        }
    }else{
        alert("Faltan datos requeridos");
    }
}