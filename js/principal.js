//validar usuario si existe
var usuarios = [];
var usuarioLogueado = [];
var usuariosSesion = JSON.parse(sessionStorage.getItem('usuarios'));
if(usuariosSesion == null){
    //usuario no definido
}else{
    //si existe un usuario, mostrar datos
    usuarios = usuariosSesion;
    usuarios.forEach(verificarSesionUsuarios);
}
function verificarSesionUsuarios(value, index, array) {
    if(value[7] == 1){//usuario logueado entonces se cargan datos
        document.getElementById("perfil").innerHTML = array[index][5];
        document.getElementById("menu_perfil").innerHTML = '<a href="perfil.html">Perfil</a>'+
            '<ul>'+
                '<li id="cerrarsesion">'+
                    '<a href="#">Cerrar Sesión</a>'+
                '</li>'+
            '</ul>';
        document.getElementById("cerrarsesion").onclick = function() { cerrarSesion(); }
        usuarioLogueado[0] = array[index][0];
        usuarioLogueado[1] = array[index][1];
        usuarioLogueado[2] = array[index][2];
        usuarioLogueado[3] = array[index][3];
        usuarioLogueado[4] = array[index][4];
        usuarioLogueado[5] = array[index][5];
        usuarioLogueado[6] = array[index][6];
        usuarioLogueado[7] = 1;
    }
}
//funcion de cerrar sesion
function cerrarSesion(){
    usuarios.forEach(function(value, index, array) {
        if(value[7] == 1){//usuario logueado entonces se cierra la sesion
            array[index][7] = 0;
            sessionStorage.setItem('usuarios', JSON.stringify(usuarios));
            window.location.href = "index.html";
        }
    });
}

//cargar rides
var rides = [];
var consecutivo = 1;
var ridesSesion = JSON.parse(sessionStorage.getItem('rides'));
if(ridesSesion == null){
    //ride no definido

}else{
    //si existe un ride
    rides = ridesSesion;
    rides.forEach(function(value, index, array) {
        consecutivo = array[index][0];
    });
    consecutivo = consecutivo + 1;
}
